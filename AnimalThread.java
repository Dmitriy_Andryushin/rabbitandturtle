public class AnimalThread extends Thread {
    public String name;
    public int priority;

    public AnimalThread(String name, int priority) {
        this.name = name;
        this.priority = priority;


    }

    public void run() {
        int y = 0;
        int countMetrs = 0;
        do {
            countMetrs++;
            if (countMetrs % 100 == 0) {
                System.out.println("Товарищ, Я " + name + ",пробежал " + countMetrs + " метров" );
            }
        } while (countMetrs < 10000);
    }
}
